<?php

namespace Croustille\Lozad;

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Croustille_Lozad
 * @subpackage Croustille_Lozad/includes
 * @author     Patrick Vézina <patrick@croustille.io>
 */
class Croustille_Lozad_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'croustille-lozad',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
