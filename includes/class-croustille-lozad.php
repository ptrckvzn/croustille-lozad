<?php

namespace Croustille\Lozad;

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Croustille_Lozad
 * @subpackage Croustille_Lozad/includes
 * @author     Patrick Vézina <patrick@croustille.io>
 */
class Croustille_Lozad {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Croustille_Lozad_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'CROUSTILLE_LOZAD_VERSION' ) ) {
			$this->version = CROUSTILLE_LOZAD_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'croustille-lozad';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_main_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Croustille_Lozad_Loader. Orchestrates the hooks of the plugin.
	 * - Croustille_Lozad_i18n. Defines internationalization functionality.
	 * - Croustille_Lozad_Admin. Defines all hooks for the admin area.
	 * - Croustille_Lozad_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-croustille-lozad-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-croustille-lozad-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-croustille-lozad-main.php';

		$this->loader = new Croustille_Lozad_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Croustille_Lozad_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Croustille_Lozad_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_main_hooks() {

		$plugin_main = new Croustille_Lozad_Main( $this->get_plugin_name(), $this->get_version() );

        $this->loader->add_filter('jpeg_quality', $plugin_main, 'set_jpeg_quality');
        $this->loader->add_action( 'wp_enqueue_scripts', $plugin_main, 'enqueue_scripts' );
        $this->loader->add_filter('wp_get_attachment_image_attributes', $plugin_main, 'attachment_image_attributes', 10, 3);
        $this->loader->add_action('after_setup_theme', $plugin_main, 'add_lozad_image_size');
        $this->loader->add_filter('the_content', $plugin_main, 'filter_img_tags', 99);
        $this->loader->add_filter('acf_the_content', $plugin_main, 'filter_img_tags', 99);

    }

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Croustille_Lozad_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
