<?php

namespace Croustille\Lozad;

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Croustille_Lozad
 * @subpackage Croustille_Lozad/includes
 * @author     Patrick Vézina <patrick@croustille.io>
 */
class Croustille_Lozad_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
