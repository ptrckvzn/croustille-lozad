<?php

namespace Croustille\Lozad;

/**
 * The main functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the main stylesheet and JavaScript.
 *
 * @package    Croustille_Lozad
 * @subpackage Croustille_Lozad/includes
 * @author     Patrick Vézina <patrick@croustille.io>
 */
class Croustille_Lozad_Main {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the JavaScript for the main side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

        if (apply_filters('croustille/use_lozad', true)) {
            wp_enqueue_script( $this->plugin_name, asset_path('scripts/main.js'), [], $this->version, false );

            /**
             * Polyfill d'IntersectionObserver
             */
            wp_enqueue_script($this->plugin_name . '/io', 'https://polyfill.io/v2/polyfill.min.js?features=IntersectionObserver', [], false, false);
        }

    }

    /**
     * Lazy load images... pas besoin de compression sur les jpeg
     *
     * @return void
     */
    public function set_jpeg_quality()
    {
        return 100;
    }

    /**
     * Ajoute une taille d'image pour le thumbnail en lazy load
     *
     * @return void
     */
    public function add_lozad_image_size()
    {
        add_image_size( 'lozad-thumbnail', 75, 9999, false );
    }

    /**
     * Filtre les attributs de wp_get_attachment_image afin d'ajouter
     * la classe 'lozad', remplacer 'src' par un thumbnail de
     * 75px de large et mettre les valeurs 'src', 'srcset'
     * sous 'data-src', 'data-srcset'.
     *
     * @param [type] $attr
     * @param [type] $attachment
     * @param [type] $size
     * @return void
     */
    public function attachment_image_attributes($attr, $attachment, $size)
    {
        if (is_admin()) {
            return $attr;
        }

        if (apply_filters('croustille/use_lozad', true)) {
            if ($size !== 'thumbnail') {
                $attr['data-src'] = $attr['src'];
                if (isset($attr['srcset'])) {
                    $attr['data-srcset'] = $attr['srcset'];
                    unset($attr['srcset']);
                }

                $thumb_size = 'lozad-thumbnail';

                $sizes = wp_get_additional_image_sizes();
                if (isset($sizes['lozad-' . $size])) {
                    $thumb_size = 'lozad-' . $size;
                }

                $attr['src'] = apply_filters('croustille/lozad_src', wp_get_attachment_image_src($attachment->ID, $thumb_size)[0]);
                $attr['class'] = join(' ', ['lozad', $attr['class']]);
            }
        }
        return $attr;
    }

    /**
     * Filtre $content et remplace les tags img par
     * des tags ajoutés de la classe 'lozad', 'data-src',
     * 'data-srcset' et place un thumbnail en 'src'
     *
     * inspiré de...
     * @see wp_make_content_images_responsive (wp-includes/media.php)
     *
     * @param [type] $content
     * @return void
     */
    public function filter_img_tags ($content)
    {
        if (!apply_filters('croustille/use_lozad', true)) {
		    return $content;
        }

        if ( ! preg_match_all( '/<img [^>]+>/', $content, $matches ) ) {
		    return $content;
	    }

	    $selected_images = $attachment_ids = array();

	    foreach( $matches[0] as $image ) {
		    if ( false === strpos( $image, ' data-srcset=' ) && 0 < strpos( $image, ' srcset=' ) && preg_match( '/wp-image-([0-9]+)/i', $image, $class_id ) &&
			    ( $attachment_id = absint( $class_id[1] ) ) ) {

                    $selected_images[ $image ] = $attachment_id;
			        // Overwrite the ID when the same image is included more than once.
                    $attachment_ids[ $attachment_id ] = true;
            }
        }

        if ( count( $attachment_ids ) > 1 ) {
            /*
            * Warm the object cache with post and meta information for all found
            * images to avoid making individual database calls.
            */
            _prime_post_caches( array_keys( $attachment_ids ), false, true );
	    }

        foreach ( $selected_images as $image => $attachment_id )
        {
            $thumb = apply_filters('croustille/lozad_src',wp_get_attachment_image_src($attachment_id, 'lozad-thumbnail'));
            $new_image = str_replace(' srcset=', ' data-srcset=', $image);
            $new_image = str_replace(' src=', sprintf(' src="%s" data-src=', $thumb[0]), $new_image);
            $new_image = str_replace(' class="', ' class="lozad ', $new_image);
            $content = str_replace($image, $new_image, $content);
        }

        return $content;
    }
}
