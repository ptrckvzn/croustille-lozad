# Croustille / Lozad.js

Implémentation de Lozad.js pour WordPress.

Filtre les images ajouté au site avec la fonction 'wp_get_attachment_image', ainsi que les images ajoutées au 'content' appellé avec `the_content()`.

## Thumbnails

Pour créer un thumbnail spécifique à une dimension d'image définie avec `add_image_size()`, créer une thumbnail au même ratio avec le préfix `lozad-`.

```
add_image_size('archive-thumbnail', 600, 400, true);
add_image_size('lozad-archive-thumbnail', 100, 66.67, true);
```

## API

### Filters

Change le thumbnail pour un gif transparent de 1px.

```
add_filter('croustille/lozad_src', function () {
    return 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
});
```

Filtre pour déterminer si lozad doit être utilisé ou non sur une page.

```
add_filter('croustille/use_lozad', function () {
    if (is_single() && get_post_type() == 'page') {
      return false;
    }

    return true;
});
```
