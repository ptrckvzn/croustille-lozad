import lozad from 'lozad';

((window) => {
  window.lozadObserver = lozad('.lozad', {
    rootMargin: '200px 0px 200px 0px',
    loaded: () => {
      document.dispatchEvent(new CustomEvent('lozad-loaded'));
    },
  });

  document.addEventListener('lozad-observe', () => {
    window.lozadObserver.observe();
  });

  document.addEventListener("DOMContentLoaded", () => window.lozadObserver.observe());
})(window)
