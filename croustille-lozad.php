<?php

namespace Croustille\Lozad;

/**
 * @wordpress-plugin
 * Plugin Name:       Croustille Lozad
 * Plugin URI:        https://bitbucket.org/ptrckvzn/croustille-lozad
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           0.5.0
 * Author:            Patrick Vézina
 * Author URI:        https://croustille.io
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       croustille-lozad
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'CROUSTILLE_LOZAD_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-croustille-lozad-activator.php
 */
function activate_croustille_lozad() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-croustille-lozad-activator.php';
	Croustille_Lozad_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-croustille-lozad-deactivator.php
 */
function deactivate_croustille_lozad() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-croustille-lozad-deactivator.php';
	Croustille_Lozad_Deactivator::deactivate();
}

register_activation_hook( __FILE__, __NAMESPACE__ . '\\activate_croustille_lozad' );
register_deactivation_hook( __FILE__, __NAMESPACE__ . '\\deactivate_croustille_lozad' );

/**
 * Load assets helper from 'dist/'
 *
 * Use: asset_path('styles/public.css');
 */
require_once plugin_dir_path( __FILE__ ) . 'includes/interface-manifest.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/class-json-manifest.php';

function asset_path ($asset) {
    static $assets;
    isset($assets) || $assets = new JsonManifest(
        plugin_dir_path( __FILE__ ) . 'dist/assets.json',
        plugin_dir_url( __FILE__ ) . 'dist'
    );
    return $assets->getUri($asset);
}

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-croustille-lozad.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_croustille_lozad() {

	$plugin = new Croustille_Lozad();
	$plugin->run();

}
run_croustille_lozad();
